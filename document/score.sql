/*
 Navicat Premium Data Transfer

 Source Server         : Ali
 Source Server Type    : MySQL
 Source Server Version : 50649
 Source Host           : 47.94.92.138:3306
 Source Schema         : score

 Target Server Type    : MySQL
 Target Server Version : 50649
 File Encoding         : 65001

 Date: 14/11/2020 17:17:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `adminAccount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员账号',
  `adminPwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for classs
-- ----------------------------
DROP TABLE IF EXISTS `classs`;
CREATE TABLE `classs`  (
  `cId` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `cName` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `cSchoolName` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`cId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `courseId` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `courseName` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `credit` int(255) NOT NULL,
  PRIMARY KEY (`courseId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lesson
-- ----------------------------
DROP TABLE IF EXISTS `lesson`;
CREATE TABLE `lesson`  (
  `lessonId` int(255) NOT NULL AUTO_INCREMENT,
  `tId` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `courseId` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '课程号',
  `schoolYear` char(9) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL COMMENT '学年：2018-2019',
  `term` int(1) NOT NULL COMMENT '学期，值为“1” or \"2\"',
  `remark1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分析',
  `remark2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `remark3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `examTime` date NULL DEFAULT NULL,
  `examStuNum` int(11) NULL DEFAULT 0 COMMENT '参加考试的人数，这是分析表中老师手动填的内容',
  `examWay` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '考试方法，这是分析表中老师手动填的内容',
  `examOrigin` int(255) NULL DEFAULT NULL COMMENT '试题源，这是分析表中老师手动填的内容',
  `entered` int(2) NULL DEFAULT 0 COMMENT '1为已录入，0为未录入',
  `scoreType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `usualProportion` double NULL DEFAULT NULL COMMENT '平时分所占比例',
  PRIMARY KEY (`lessonId`) USING BTREE,
  INDEX `teacher`(`tId`) USING BTREE,
  CONSTRAINT `teacher` FOREIGN KEY (`tId`) REFERENCES `teacher` (`tId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1013 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compressed;

-- ----------------------------
-- Table structure for revocation
-- ----------------------------
DROP TABLE IF EXISTS `revocation`;
CREATE TABLE `revocation`  (
  `revocationId` int(11) NOT NULL AUTO_INCREMENT,
  `revocationReason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '撤销理由',
  `revocationType` int(255) NOT NULL,
  `removeLessonId` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `revocationReply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0',
  PRIMARY KEY (`revocationId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `scoreId` int(255) NOT NULL AUTO_INCREMENT,
  `lessonId` int(255) NOT NULL COMMENT '课程的ID',
  `normalScore` int(255) NULL DEFAULT -1 COMMENT '正考成绩，默认为-1，十分制0~100，等级制0~4',
  `supplementScore` int(255) NULL DEFAULT -1 COMMENT '补考成绩，没有补考则为-1',
  `clearScore` int(255) NULL DEFAULT -1 COMMENT '清考成绩，没有清考则为-1',
  `usualScore` int(255) NULL DEFAULT -1 COMMENT '平时成绩，默认为-1',
  `studentId` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '学生的ID',
  PRIMARY KEY (`scoreId`) USING BTREE,
  INDEX `lesson`(`lessonId`) USING BTREE,
  INDEX `stu`(`studentId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 428 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `sId` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `sName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `sPwd` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `sEmail` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `cId` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`sId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `tId` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `tName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `tPwd` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `tSchoolName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`tId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Triggers structure for table score
-- ----------------------------
DROP TRIGGER IF EXISTS `t1`;
delimiter ;;
CREATE TRIGGER `t1` AFTER INSERT ON `score` FOR EACH ROW #固定写法，为的是批量操作
begin
    update lesson	set lesson.examStuNum = lesson.examStuNum +1 where lesson.lessonId=new.lessonId;
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table score
-- ----------------------------
DROP TRIGGER IF EXISTS `t2`;
delimiter ;;
CREATE TRIGGER `t2` AFTER DELETE ON `score` FOR EACH ROW #固定写法，为的是批量操作
begin
    update lesson	set lesson.examStuNum = lesson.examStuNum -1 where lesson.lessonId=old.lessonId;
end
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
